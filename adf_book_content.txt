
Go though Dev Guide and build the usecase in the same sequence
============================================================

Introduction

A bridge course for Java EE developers

Architecture 

Java EE VS ADF 

Model Driven Development

Building Business Servcies using ADF Business Components

ApplicationModule
	Introduction
	



Connection

Entity Objects

View Objects

Validation

Transaction

ViewLink

Security

Scalabiliy, Clustering and Pooling

Fine Tuning the ADF Business Components

Binding Business Services with UI

Exception Handling

Lifecycle of a page

ADF Controller and Taskflow

Modularizing your Application

Packaging, Distribution and Deployment
Resuing AM

Unit Testing
EJB / WebServices

Building LOV
Building Query Component
Control Hints
----------------------------------------------------------------------

ADF BC

EntityObject

  Introduction
  	Table Mapping
  Lifecycle
  Attribute Control hints
  Using ResourceBundles
  Primary Key/Alt Key
  Declaratively Populating Attributes from an Oracle Sequence 
  Validation
  	Understanding the Validation Cycle
  	Implementing Validation and Business Rules Programmatically
  	Efficiently Enforcing Uniqueness of Non-Primary Attribute Value
  	Groovy Support
  Association
  Programatic Entity
  	EntityObject based on InstedOf trigger
  	EntityObject using Stored Procedure
  	EntityObject based on non DB DataSource
  Locking
  Groovy Support
  	Attribute Value Initialiation
  	Groovy in validation
  ViewAccessor
  Creating Custom Domain
  Business Group
  Entity Operation Flag
  	Soft Deletion
  	Set Deleted Flag Instead of Actually Deleting a Row
  Handling BLOB/CLOB/ORDImage
  Batch Processing
  

  
ViewObject
  Introduction
  Types of VO
  Query using ViewObject
  Ingredients Of VO
       	Attribute Control hints
  	ViewCriteria 
  	BindVariable
  	VieLink
  
  VO based on multiple EO
  ViewObject Ststic Values-usecase
  Accessing ResourceBundles
  Programmatic VO
  	Validation at VO level
  	Programatic ViewLink
  	
  Cutomizing Query Generation
  	Overriding getCriteriaItemCluase  	
  	ViewCriteriaAdaptor
  	
  Dynamic ViewObject
	ViewObject built from SQL  
  Working with View Object Query Results

  Sorting
  	Sorting Programtioc VO basedon EO
  	Sorting Transient VO
  ViewAccessor
  VO from XML
  	ReadXML/WriteXML
  LOV / Cascading LOV
  Tuning ViewObject
  Further Filtering View Link Accessor RowSets at Runtime
  Apply Bind Varible Values to Filter View Link Accessor RowSets 
  Using Comma-Separated String Bind for Variable IN List
  Joining Static Array of Database Type Data into View Object Query
  Implementing Custom View Row Hint Behavior
  Chapter 6, "Working with View Object Query Results"
  Update EO through VO
  
Application Module

   Introduction	
   Ingredients Of Application Module
   	ViewObjects and ViewLinks
   	Methods
   	ApplicationModule
   Lifecycle Phases
   	Activation/Passivation
   	Lifecycle Methods
   	Make variables/state passivation safe
   Setting up the runtime context
   	ADF Context 
   	Timezone / Locale
   Connecion Mangement
   	JDBC Data Source
   	Connection Pooling
   	Passing Dynamic Credentials
   Transaction
   	Managing Transaction
   	Custom Transaction
   Application With No Database Connection
   Configure ApplicationModule for Optimal Usage
   Nesting Application Module
   Save a snapshot in memory
   Sharing Application - Sessions
   Reusability
   Locking 
   Adding Dynamism
   	Creating ViewObjects on the fly
   	Defining ViewLinks 
   Calling StoredProcedures
   Enable Asynchronus Invokation using MDB
   Service-Enabled Application Module
   Integrating Service-Enabled Application Modules
   Accessing AM from Client
   	Stateful invokation of ApplicationModule from a Servlet
   	Stateless invokation of ApplicationModule
   
    
ADF Model
	
  Why binding layer?
  JSR 227 at a glance
  Building a simple page
  Ingredeints of ADFm
  	cpx
  	dcx
  	adfm
  	pagedef
  Different Parts of binding container- pagedefinition
  	parameters
  	executables
  	bindings
  	events
  	action ...
  Nested BindingContainer  	
  Detailed analysis of each part
  Skipping Validation
  Nesting Binding Container
  Calendar Binding
  InvokeAction Issues alternatives
  ListOfValues binding
  Custom Error Handler
  Page Controller
  Error Reporting Issues when bypass binding layer
  Creating Custom DataControl   
  Binding to Domain
  Accessing AM from servlet
  Creating a Dynamic Attribute Binding for a Dynamic View Object Attribute
  Automatically Disabling Eager Coordination of Details Not Visible on Current Page [10.1.3]
  ADF DataControl
  	Subclassing the ADFBC Data Control to Override begin/end-Request Method [10.1.3]
  	Avoiding Dirtying the ADF Model Transaction When Transient Attributes are Set
 	Custom DataControl
 	
Using ADF Model Data Binding in a Java EE Web Application
	EJB
		Introduction to Building Java EE Web Applications with Oracle ADF
		Using ADF Model Data Binding in a Java EE Web Application
		Creating a Basic Databound Page
		Creating ADF Databound Tables
		Displaying Master-Detail Data
		Creating Databound Selection Lists
		Creating Databound Search Forms
		Deploying an ADF Java EE Application
	Webservice
	  	Chapter 13, "Integrating Web Services Into a Fusion Web Application"
  		Entity and View Object Based on Web Service
	
	POJO DataControl
  
ADF Security 


ADF Controller

  Taskflow lazy initialization
  Taskflow init params
  Taskflow UI shell design pattern
  Accesing security Context through EL
  Checking access through EL
  Nested Transaction Hnalding Declarativley
  Refreshing Parent View When a taskflow returns


View
  Creating a Databound Web User Interface
  Passing Multiple Selected Keys from Tree, Table, and TreeTable to Application Module Method
  Passing RowIterator of Selected Tree Node to AM Method
  Programmatically Displaying Task Flows in the ADF UI Shell
  Customizing Applications with MDS
  
Best Practices

Testing and Debugging

Include Commonly Used Methods
  